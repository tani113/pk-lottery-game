﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

//タッチ・ドラッグ・ピンチ操作に対する処理
public class FingerAction : MonoBehaviour {

    bool nowUGuiTouch; //uGUIにタッチしている間はtrue
    bool nowPinch; //ピンチイン・ピンチアウトをしている間はtrue
    bool nowPieceTouch; //コマにタッチしている間はtrue
	GameObject camera;
    MyCanvas myCanvas;

    //float nowTime = 0;

    void Start() {
		camera = GameObject.FindGameObjectWithTag("MainCamera");
        myCanvas = GameObject.Find("Canvas").GetComponent<MyCanvas>();
    }

    void Update() {
        /*  ボールシュート処理 */
		if (Global.isSlideStart) {
            //Debug.Log("ボール移動開始！！");
            if(Global.isSlideActionActive) {
                ShowLineAndSilhouette();
            }
            Global.isSlideActionActive = false;

            //ShowLineAndSilhouette();
            Global.ballWaitTime += Time.deltaTime;
            if (Global.ballWaitTime > 0.2f) {
                if(isShowKickANdLine) {
                    myCanvas.SetActive("KickSilhouette", false);
                    myCanvas.SetActive("IntensiveLine", false);
                    myCanvas.SetActive("GameGuide", false);
                    isShowKickANdLine = false;
                }
                Vector3 ballPos = Global.ball.transform.position;
                if (ballPos.z > -5.9f) {
                    float ballY = 0.06f; //0.12f
                    if (ballPos.z < -5.0f) ballY = -ballY; //-5.1f
                    Global.ball.transform.position += new Vector3(0, ballY, -0.045f); // input value -0.045f
                }
                else {
                    Global.isSlideStart = false;
                    Global.ballWaitTime = 0f;
                    //Debug.Log("=======ボール移動終了");
                }
            }

        }
    }

    bool isShowKickANdLine = false;

    //集中線とシルエットを表示
    void ShowLineAndSilhouette() {
        myCanvas.SetActive("KickSilhouette", true);
        myCanvas.SetActive("IntensiveLine", true);
        isShowKickANdLine = true;
    }

    //ピンチイン・ピンチアウトによる画面のズームイン・アウト
    void OnPinch(PinchGesture gesture) {
        if (Global.selectMyNo >= 0 && Global.nowMoveMe == false) {
            return; //選手選択が反応している場合はピンチイン・ピンチアウトは動作させないようにする
        }
        ContinuousGesturePhase phase = gesture.Phase; // 現在のgesture phase (Started/Updated/Ended)
        if (phase == ContinuousGesturePhase.Updated) {
            nowPinch = true;
        } else if (phase == ContinuousGesturePhase.Ended) {
            nowPinch = false;
            return;
        }
        float gap = gesture.Gap; // 2指間の現在の距離
        float delta = -gesture.Delta; // 前回のフレームとの差(ピンチイン・ピンチアウトの挙動が逆だったのでマイナスをつけた)
        //Debug.Log("ピンチイン・ピンチアウト発生 gap:" + gap + " delta:" + delta);
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
        Vector3 cameraPos = camera.transform.position;
        if (gap > 0.001) {
            if ((0 < delta && cameraPos.y < 5) || //カメラY位置が5以下なら高度Yを上げる処理が可能
                (delta < 0 && 1.5 < cameraPos.y)) { //カメラY位置が1.5以上なら高度Yを下げる処理が可能
                camera.transform.position += new Vector3(0, delta / 100, 0);
            }
        }

    }

    //ドラッグによる描写画面のスライド
    void OnDrag(DragGesture gesture) {
		if (Global.isSlideActionActive) {
			Global.isSlideStart = true;
		} else {
//			Debug.Log("Now can't shoot ball");
		}

    }

}
