﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonActions : MonoBehaviour {

    bool nowTouchUp, nowTouchDown; //それぞれUpボタン、Downボタン押下中ならtrue

    Sprite levelOpen; //CPU難易度選択スプライト画像
    MyCanvas myCanvas;
    Image upButton, downButton, gameNextButton; //ボタン画像オブジェクト
    
    void Start() {
        myCanvas = GameObject.Find("Canvas").GetComponent<MyCanvas>();
    }

	float cameraUpMax = 357f; //Up Button カメラUp上限下限 50f
	float cameraDownMax = 8f; //Down Button カメラ回転上限 80f

    void Update() {
        if (Global.mainCamera == null) return;
		//試合中のボタンのアクティブ判定
		float cameraAngleX = Global.mainCamera.transform.eulerAngles.x;
//		Debug.Log("test: eulerAngles.x:"+ cameraAngleX);
		if (upButton != null && Global.mainCamera != null) {
			if (cameraAngleX > cameraUpMax || cameraAngleX < (cameraDownMax + 10) ) {
				upButton.enabled = true;
			} else {
				upButton.enabled = false;
//				upButton.enabled = true;
			}
		}

        if (downButton != null && Global.mainCamera != null) {
			if (cameraAngleX > (cameraUpMax -10) || cameraAngleX < cameraDownMax) {
                downButton.enabled = true;
            } else {
                downButton.enabled = false;
//				downButton.enabled = true;
            }
        }

        //画面の縦回転
        if (nowTouchUp) { //上回転
            UpView();
        }
        if (nowTouchDown) { //下回転
            DownView();
        }

    }

    //設定画面の前のパスコード入力画面へ
    public void GoPasscodePopup() {
        Debug.Log("GoPasscodePopup開始");
        myCanvas.SetActive("PasscodePopup", true);
        myCanvas.SetActive("ExplainBox", false);
        Global.passcode = -1;
    }

    //パスコード表示を閉じる
    public void ClosePasscodePopup() {
        if (GameObject.Find("PasscodePopup/ShowInputZone/Text") != null) {
            GameObject.Find("PasscodePopup/ShowInputZone/Text").GetComponent<Text>().text = "";
        }
        myCanvas.SetActive("PasscodePopup", false);
        myCanvas.SetActive("PasscodeNGPopup", false);
        Global.passcode = -1;

        if (Global.isGameStop) {
            myCanvas.SetActive("ExplainBox", true);
        }
    }

    //パスコードのチェック。passcodeの-1は数値が入力されていない状態を表す
    //クリアボタンが押されたら、inputNumberが-1となる
    public void InputPasscode(int inputNumber) {
        if (Global.passcode >= 1000) return;
        if (inputNumber == -1) {
            Global.passcode = inputNumber;
            GameObject.Find("PasscodePopup/ShowInputZone/Text").GetComponent<Text>().text = "";
            return;
        }
        if (Global.passcode == -1) {
            //数値入力前
            Global.passcode = inputNumber;
            GameObject.Find("PasscodePopup/ShowInputZone/Text").GetComponent<Text>().text = inputNumber.ToString();
        } else {
            Global.passcode = Global.passcode * 10 + inputNumber;
            GameObject.Find("PasscodePopup/ShowInputZone/Text").GetComponent<Text>().text = Global.passcode.ToString();
            if (Global.passcode == Global.TRUEPASS) {
                GoSettingView();
            } else {
                if(Global.passcode >= 1000) {
                    myCanvas.SetActive("PasscodeNGPopup", true);
                }
            }
        }
    }

    public void ClosePasscodeProblemPopup() {
        myCanvas.SetActive("PasscodeNGPopup", false);
        Global.passcode = -1;
        if(GameObject.Find("PasscodePopup/ShowInputZone/Text") != null) {
            GameObject.Find("PasscodePopup/ShowInputZone/Text").GetComponent<Text>().text = "";
        }
    }

    //パスコードが通過し、ゲーム画面から設定画面に
    public void GoSettingView() {
        myCanvas.SetActive("ExplainBox", false);
        StartCoroutine(WaitToMoveSetting(0.3f));
    }

    public IEnumerator WaitToMoveSetting(float waitSeconds) {
        yield return new WaitForSeconds(waitSeconds); //設定seconds秒数立ってから下の処理を行う
        myCanvas.SetActive("PasscodePopup", false);
        Application.LoadLevel(Global.Scenes.GameSetting.ToString());
        Global.ResetAll();
    }

    //試合中の画面から設定画面に
    public void OnGameView()
    {
        Application.LoadLevel(Global.Scenes.GameMain.ToString());
        myCanvas.SetEnableBtn("GoLotButton", false);
    }

    //次のキックに行く
    //Touch next game button
    public void OnNextGame() {
        Debug.Log("next gameボタン押下後処理");
        myCanvas.SetActive("GameNextButton", false);
        if (Global.gameCount == Global.GameCount.Next) {
            Global.selectHitNo(); //次の抽選を行う
            if (Global.isGameStop) {
                myCanvas.SetActive("ExplainBox", true);
                return;
            } else {
                Global.gameCount = Global.GameCount.First;
                GameScoreImageReset();
            }
        }
        Global.ball.transform.position = new Vector3 (0, 0, -4.3f);
		Global.isStopPanel = false;
		foreach (GameObject slotPanel in Global.slotPanels) {
			slotPanel.SetActive(true);
			slotPanel.GetComponent<RotateSlotPanel>().ResetStatus();
		}
        if(Global.gameCount == Global.GameCount.FirstStop) {
            Global.gameCount = Global.GameCount.Second;
        } else if(Global.gameCount == Global.GameCount.SecondStop) {
            Global.gameCount = Global.GameCount.Third;
        }

        //camera move to start position
        GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
		Vector3 cameraEnd = new Vector3 (0f, 0.44f, -3.77f);
		iTween.MoveTo(camera, iTween.Hash("position", cameraEnd, "time", 3.0f,
		                                  "oncomplete", "ResetCameraPos", "oncompletetarget", this.gameObject)); 
        /* !!!!!!!!!!!!!!!!!!!!デバッグ用に変更中！！！！！！！！！！！！！ */
		StartCoroutine(SlideActive (Global.shootTempo) ); //1.7秒後に次のシュートが可能になるのが正規 
    }

    //合計得点表示画像を閉じる
    public void TotalScoreClose() {
        Debug.Log("TotalScoreClose開始");
        myCanvas.SetActive("TotalScoreImage", false);
        myCanvas.SetActive("TotalScoreMascot", false);
        myCanvas.SetActive("GameNextButton", true); //次へボタン表示
        //Global.gameCount = Global.GameCount.First;
        Global.sumScore = 0;
        //GameScoreImageReset();
        myCanvas.SetActive("TotalScoreFrame", false);
    }

    //上部のスコア表示を初期化
    void GameScoreImageReset() {
        for (int i = 1; i < 4; i++) {
            Global.SpriteImageSet(
                "soccerMinigame/blueScore/0alpha", //使用画像パス
                 "Canvas/" + i + "ScoreOne"); //画像変更対象オブジェクトパス
                                              //合計2桁目
            Global.SpriteImageSet(
                "soccerMinigame/blueScore/0alpha", //使用画像パス
                "Canvas/" + i + "ScoreTen"); //画像変更対象オブジェクトパス
        }
        Global.SpriteImageSet(
            "soccerMinigame/yellowScore/0alpha", //使用画像パス
             "Canvas/TotalScoreOne"); //画像変更対象オブジェクトパス
                                      //合計2桁目
        Global.SpriteImageSet(
            "soccerMinigame/yellowScore/0alpha", //使用画像パス
            "Canvas/TotalScoreTen"); //画像変更対象オブジェクトパス

    }

    public void ResetHitLotPopup() {
        myCanvas.SetActive("DecideBox", true);
        myCanvas.SetActive("CancelButton", true);
        GameObject.Find("DecideBox/Text").GetComponent<Text>().text = "\n出現数のリセットを行いますか？";
        myCanvas.SetActive("HitLotResetBtn", true);
        myCanvas.SetActive("SetAndHitLotResetBtn", false);
    }

    public void ResetHitLot() {
        for (int i = 1; i < 11; i++) {
            Global.hitLot[i] = 0;
            PlayerPrefs.SetInt("hitLot" + i, 0);
        }
        Global.hitLotAll = 0;
        PlayerPrefs.SetInt("hitLotAll", 0);
        Global.SetLotNoAndAmountText();
        SettingPopupClose();
    }

    public void ResetSetAndtHitLotPopup() {
        myCanvas.SetActive("DecideBox", true);
        myCanvas.SetActive("CancelButton", true);
        GameObject.Find("DecideBox/Text").GetComponent<Text>().text = "\n設定本数と出現数のリセットを行いますか？";
        myCanvas.SetActive("HitLotResetBtn", false);
        myCanvas.SetActive("SetAndHitLotResetBtn", true);
    }

    public void ResetSetAndHitLot() {
        ResetHitLot();
        for (int i = 1; i < 11; i++)
        {
            Global.setLot[i] = 0;
            PlayerPrefs.SetInt("setLot" + i, 0);
        }
        Global.setLotAll = 0;
        PlayerPrefs.SetInt("setLotAll", 0);
        Global.SetLotNoAndAmountText();
        SettingPopupClose();
    }

    public void SettingPopupClose() {
        myCanvas.SetActive("DecideBox", false);
        myCanvas.SetActive("CancelButton", false);
        myCanvas.SetActive("HitLotResetBtn", false);
        myCanvas.SetActive("SetAndHitLotResetBtn", false);
    }

    public void StartLotNoSet() {
        Global.nowLotNoSet = true;
        Global.nowLotAmountSet = false;
        GameObject.Find("LotNoButton").GetComponent<Image>().color = new Color(44f / 255.0f, 176.0f / 255.0f, 219f / 255.0f, 255f / 255.0f);
        GameObject.Find("LotAmountButton").GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
    }

    public void StartLotAmountSet()
    {
        Global.nowLotNoSet = false;
        Global.nowLotAmountSet = true;
        GameObject.Find("LotNoButton").GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
        GameObject.Find("LotAmountButton").GetComponent<Image>().color = new Color(44f / 255.0f, 176.0f / 255.0f, 219f / 255.0f, 255f / 255.0f);
    }

    public void InputNumber(int inputNumber) {
        if(Global.nowLotNoSet) {
            String inputNumberStr = inputNumber.ToString();
            if (inputNumber == 0) {
                if(Global.allLot < 10 || Global.inputLotNo != 1) return;
                if(Global.allLot == 10) {
                    Global.inputLotNo = 10;
                    inputNumberStr = "10";
                }
            } else {
                Global.inputLotNo = inputNumber;

            }

            //GameObject lotNoButton = GameObject.Find("LotNoButton");
            if (inputNumber == -1) inputNumberStr = "--";
            GameObject.Find("LotNoButton/Text").GetComponent<Text>().text = inputNumberStr;
        } else if(Global.nowLotAmountSet) {
            //if (inputNumber == 0 && Global.inputLotAmount < 10) return;
            if (Global.inputLotAmount > 100000 && inputNumber != -1) return;
            String inputLotAmountStr = "NG";
            if (inputNumber == -1) {
                Global.inputLotAmount = -1;
                inputLotAmountStr = "----";
            } else {
                if (Global.inputLotAmount != -1) {
                    Global.inputLotAmount = Global.inputLotAmount * 10 + inputNumber;
                } else {
                    Global.inputLotAmount = inputNumber;
                }
                inputLotAmountStr = Global.inputLotAmount.ToString();
            } 
            Debug.Log("InputNumber: " + Global.inputLotAmount);
            GameObject.Find("LotAmountButton/Text").GetComponent<Text>().text = inputLotAmountStr;
        }
    }

    public void SetLotAndNumber() {
        if(Global.inputLotNo == -1 || Global.inputLotAmount == -1) {
            return;
        } else {
            // {Global.allLot}等までなら反映
            if (Global.inputLotNo <= Global.allLot)
            {
                PlayerPrefs.SetInt("setLot" + Global.inputLotNo, Global.inputLotAmount);
                Debug.Log("SetLotAndNumber lotNo:" + Global.inputLotNo + " lotAmount:" + Global.inputLotAmount + " a:" + Global.setLot[Global.inputLotNo]);
                Global.setLot[Global.inputLotNo] = Global.inputLotAmount;
                Global.SetLotNoAndAmountText();
            } else {
                Debug.Log( (Global.allLot + 1) + "等以降は存在しないので反映しない");
            }

            Global.inputLotNo = -1;
            GameObject.Find("LotNoButton/Text").GetComponent<Text>().text = "--";
            Global.inputLotAmount = -1;
            GameObject.Find("LotAmountButton/Text").GetComponent<Text>().text = "----";
            GameObject.Find("LotNoButton").GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
            GameObject.Find("LotAmountButton").GetComponent<Image>().color = new Color(1f, 1f, 1f, 1f);
        }
    }
    	
	public IEnumerator SlideActive(float waitSeconds) {
		yield return new WaitForSeconds(waitSeconds); //設定seconds秒数立ってから下の処理を行う
        myCanvas.SetActive("GameGuide", true);
        Global.isSlideActionActive = true;
	}

    /*     以下、現在未使用       */

    /* Upボタン関連 */
    //Upボタンタッチ中の処理（画面の上回転）
    public void UpView() {
		float cameraAngleX = Global.mainCamera.transform.eulerAngles.x;
		if (cameraAngleX > cameraUpMax || cameraAngleX < (cameraDownMax + 10)) {
			Global.mainCamera.transform.Rotate (new Vector3 (-1f, 0, 0));
		}
    }
    //Upボタンをタッチ開始した時の処理
    public void OnUpButtonStart() {
        nowTouchUp = true;
    }
    //Upボタンのタッチが終了した時の処理
    public void OnUpButtonStop() {
        nowTouchUp = false;
    }

    /* Downボタン関連 */
    //Downボタンタッチ中の処理（画面の下回転）
    public void DownView() {
		float cameraAngleX = Global.mainCamera.transform.eulerAngles.x;
		if (cameraAngleX > (cameraUpMax -10) || cameraAngleX < cameraDownMax) {
			Global.mainCamera.transform.Rotate(new Vector3(1f, 0, 0));
		}
    }
    //Downボタンをタッチ開始した時の処理
    public void OnDownButtonStart() {
        nowTouchDown = true;
    }
    //Downボタンのタッチが終了した時の処理
    public void OnDownButtonStop() {
        nowTouchDown = false;
    }

    //共通処理
    void GeneralProcess() {
        Global.nowPlayerTweenActive = false;
        Global.nowCpuTweenActive = false;
        if (Global.IsMeOwner()) {
            Global.ball.rigidbody.isKinematic = false;
            Global.ball.rigidbody.velocity = Vector3.zero;
            Global.ball.rigidbody.angularVelocity = Vector3.zero;
            Global.ball.rigidbody.isKinematic = true;
        }
    }

    //マイページ遷移ボタン押下
    public void OnMypage() {
        Debug.Log("OnMypageボタン押下");
        Application.LoadLevel(Global.Scenes.GameMypage.ToString());
    }

	/* 								setting page button 							*/

	public void OnLotNo() {
		Debug.Log ("OnLotNo start");
	}
}
