﻿using UnityEngine;
using UnityEngine.UI;
using System; 
using System.Collections;

public class GameManager : MonoBehaviour {

    MyCanvas myCanvas;
    Action checkUserData; //GameStartのメソッド

    float resultWaitTime;
    float restartWaitTime;

    void Start() {
        myCanvas = GameObject.Find("Canvas").GetComponent<MyCanvas>();
        Global.slotPanels = GameObject.FindGameObjectsWithTag("SlotPanel");
        //設定ロトカウント
        for (int i = 1; i < 21; i++)
        {
            Global.setLot[i] = PlayerPrefs.GetInt("setLot" + i, 0); //第二引数は値が無い場合に返す値
        }
        //抽選済みロトカウント
        for (int i = 1; i < 21; i++)
        {
            Global.hitLot[i] = PlayerPrefs.GetInt("hitLot" + i, 0); 
        }
        Global.SetLotNoAndAmountText();

        Global.selectHitNo();
        if(Global.isGameStop == true && Application.loadedLevelName == Global.Scenes.GameMain.ToString()) {
            myCanvas.SetActive("ExplainBox", true);
        } else {
            Global.isStopPanel = false;
            if(Application.loadedLevelName != Global.Scenes.GameMain.ToString()) {
                myCanvas.SetActive("ExplainBox", false);
            }
        }

        return;
    }

    void Update() {
        GameSumScore();

		//return;
        //Androidでバックキーを押した際の処理
        if (Application.platform == RuntimePlatform.Android && Input.GetKey(KeyCode.Escape)) {
            if (Application.loadedLevelName == Global.Scenes.GameMain.ToString()) {
                myCanvas.SetActive("GameBackMode", true); //試合中は、試合中止の有無を確認
            } else if (Application.loadedLevelName == Global.Scenes.GameStart.ToString() ) {
                Application.Quit(); //アプリ終了
            } else if (Application.loadedLevelName == Global.Scenes.GameRule.ToString() ) {
                if (Global.gameType == 1) {
                    Application.LoadLevel(Global.Scenes.GameStart.ToString());
                } else {
                    Global.LoadBackHomeScene();
                }
            } else if (Application.loadedLevelName == Global.Scenes.GameTeamSelect.ToString() ) {
                Global.LoadBackHomeScene();
            }
        }
    }

    void GameSumScore() {
        if (Global.gameCount == Global.GameCount.End) {
            resultWaitTime += Time.deltaTime;
            if (resultWaitTime > 2f) {
                myCanvas.SetActive("TotalScoreFrame", true);
                myCanvas.SetActive("TotalScoreImage", true);
                myCanvas.SetActive("TotalScoreMascot", true);
                Global.SpriteImageSet(
                    "soccerMinigame/totalScore/" + Global.nextHitScore, //使用画像パス
                    "Canvas/TotalScoreImage"); //画像変更対象オブジェクトパス
                resultWaitTime = 0f;

                if (Global.hitLot[Global.nextHitLotNo] < Global.setLot[Global.nextHitLotNo]) {
                    Global.hitLot[Global.nextHitLotNo] += 1;
                }
                PlayerPrefs.SetInt("hitLot" + Global.nextHitLotNo, Global.hitLot[Global.nextHitLotNo]);
                int hitLotAll = 0;
                for (int i = 1; i < 11; i++) {
                    Global.hitLot[i] = PlayerPrefs.GetInt("hitLot" + i, 0); //第二引数は値が無い場合に返す値
                    hitLotAll += Global.hitLot[i];
                }
                PlayerPrefs.SetInt("hitLotAll", hitLotAll);
                Global.gameCount = Global.GameCount.Next;

                //Global.selectHitNo(); //次の抽選を行う
                //if (Global.isGameStop == true && Application.loadedLevelName == Global.Scenes.GameMain.ToString()) {
                //    myCanvas.SetActive("ExplainBox", true);
                //} 
            }
        }
    }

    void GameScoreImageReset() {
        for (int i = 1; i < 4; i++) {
            Global.SpriteImageSet(
                "soccerMinigame/blueScore/0alpha", //使用画像パス
                 "Canvas/"+ i +"ScoreOne"); //画像変更対象オブジェクトパス
                             //合計2桁目
            Global.SpriteImageSet(
                "soccerMinigame/blueScore/0alpha", //使用画像パス
                "Canvas/" + i + "ScoreTen"); //画像変更対象オブジェクトパス
        }
        Global.SpriteImageSet(
            "soccerMinigame/yellowScore/0alpha", //使用画像パス
             "Canvas/TotalScoreOne"); //画像変更対象オブジェクトパス
                                      //合計2桁目
        Global.SpriteImageSet(
            "soccerMinigame/yellowScore/0alpha", //使用画像パス
            "Canvas/TotalScoreTen"); //画像変更対象オブジェクトパス

    }

    //Android、iOSともにホームボタン(または電源ボタン)を押した際の処理
    void OnApplicationPause(bool pauseStatus) {
        if (pauseStatus) {
            //ホームボタンを押してアプリがバックグランドに移行した時
            Debug.Log("バックグランドに移行");
            if (Application.loadedLevelName == Global.Scenes.GameMain.ToString() &&
                Global.isPlayOnlineGame && Global.nowGameEnd == false) {
                Global.GameReset(); //初期化用処理
                Application.LoadLevel(Global.Scenes.GameStart.ToString());
            }
        } else {
            //アプリを終了しないでホーム画面からアプリを起動して復帰した時
            Debug.Log("バックグランドから復帰");
            if (Application.loadedLevelName == Global.Scenes.GameMypage.ToString()) {
                if (checkUserData != null) {
                    checkUserData();
                }
            }

        }
    }

}