﻿using UnityEngine;
using System;
using System.Collections;

/// <summary>
/// ボール動作とパネルに当たったあとのパネル処理。
/// </summary>
public class BallProcess : MonoBehaviour {

    GameObject selectCircle; //コマ選択時の表示
    bool isShowSelectShot; //コマ選択時のショットが表示されているか

    float deltaBallPosX, deltaBallPosZ; //ボールの移動X,Z座標差分。カメラ追尾時用

    Vector3 viewportFirst; //タッチ開始位置(ビューポート)
    Vector3 viewportCurrent;
    Vector3 viewportEnd;

    Vector3 touchStartPoint; //タッチ開始位置
    Vector3 touchCurrentPoint; //タッチ継続時の現在位置
    float touchLength; //タッチ開始位置と終了位置間の距離
    // public static float pieceMoveDistance; //プレイヤーコマ移動時の移動距離
    float pieceEndToBall; //コマ移動先位置とボール位置の距離差
    Vector3 pieceMoveEndPos; //飛ぶ位置と力を表現するショットラインの先端位置(実際にプレイヤーコマが飛ぶ位置)

    bool isPieceMoveCount;//攻撃コマの移動時間をカウント開始時にtrue、終了後はfalse
    public static float pieceMoveTimeCount; //攻撃コマの移動時間をカウント

    bool nowFisrtNearZone; //ドラッグした際に、近接位置から出るまではtrue
    public static bool nowPlayerHitBall; //プレーヤーがボールに接触した場合にtrue

    MyCanvas myCanvas;

    const float CanselLength = 0.04f; //コマのショットを中断する距離

    void Start() {
        myCanvas = GameObject.Find("Canvas").GetComponent<MyCanvas>();
    }

    void Update() {
    }

    //ボールが他オブジェクトと衝突した瞬間に呼ばれる。引数collisionは衝突相手のオブジェクト
    void OnCollisionEnter(Collision collision) {
		Debug.Log ("BallProcess ボール接触対象:" + collision.gameObject.name);
        GameObject hitPanel = collision.gameObject;
		if (hitPanel.tag == "SlotPanel" ) { //ボールとSlotPanelが衝突
            Debug.Log("BallProcess ボールがSlotPanelと接触");
			bool check = DeleteAllPanel();
            if (check == false) return;
			 
            Global.nowBallTweenActive = false;
            iTween.Stop(collision.gameObject, "move");
            Global.ballLastHitSide = Global.Side.My;
            Global.nowMoveBall = true;
            BallMoveSetting(collision); //衝突時のボール処理

        }
    }

	// all slotPanel delete and set hit slotPanel
    // パネルに当たったあとのヒットパネル表示処理
	bool DeleteAllPanel() {
		Global.isStopPanel = true; //stop all panel
		//Global.slotPanels = GameObject.FindGameObjectsWithTag("SlotPanel");
        int hitNo = Global.firstScore;
        if (Global.gameCount == Global.GameCount.Second) {
            hitNo = Global.secondScore;
            Debug.Log("2回目の当選:"+hitNo);
        } else if (Global.gameCount == Global.GameCount.Third) {
            hitNo = Global.thirdScore;
            Debug.Log("3回目の当選:" + hitNo);
        } else if(Global.gameCount == Global.GameCount.First) {
            hitNo = Global.firstScore;
            Debug.Log("1回目の当選:" + hitNo);
        } else {
            Debug.Log("DeleteAllPanel 問題発生! hitNo:" + hitNo);
            return false;
        }
        GameObject test = null;
        foreach (GameObject slotPanel in Global.slotPanels) {
            int number = slotPanel.GetComponent<RotateSlotPanel>().number;
            if(number == hitNo) {
                test = slotPanel;
                test.SetActive(true);
                Debug.Log("ヒットパネル:"+test);
            } else {
                slotPanel.SetActive(false);
            }
		}
        test.GetComponent<RotateSlotPanel>().HitScore(); //パネルにボールが当たったときの処理
        test.transform.position = new Vector3 (0f, 0.3f, -5.7f);
		//camera move
		GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
		Vector3 cameraEnd = new Vector3 (0f, 0.47f, -5.32f);
		iTween.MoveTo(camera, iTween.Hash("position", cameraEnd, "time", 1.5f,
			"oncomplete", "CameraToGoal", "oncompletetarget", this.gameObject)); //カメラ移動 time = 1.5f
        return true;
	}

    //カメラがゴール前へのズームインを終えたタイミング(ボールがパネルに当たったあと)
    void CameraToGoal() {
        if (Global.gameCount != Global.GameCount.End)
        {
            myCanvas.SetActive("GameNextButton", true);
        }
	}

    //ボールとパネルの衝突時のボール動作処理
    void BallMoveSetting(Collision collision) {
        Debug.Log("BallMoveSetting 得点パネルによるボール動作処理");
        Vector3 hitPiecePos = collision.transform.position; //衝突時のPanel位置
        Vector3 ballPos = transform.position; //衝突時のボール位置
        string hitName = collision.gameObject.name;
        string myPieceName = Global.MyPieceNameBase; //自分のコマ名のベースは不変
        float weight = 0; //コマの勢い(コマ到達予定位置とボール位置の差異)に対応した重み
        bool isReflectBall = false;
        if (hitName == myPieceName + Global.selectMyNo) {
        } else {
            Debug.Log("跳ね返り処理 name:"+hitName);
            weight = Global.ReflectWeight;//コマとボールの跳ね返り(ショットでない)時処理
            isReflectBall = true;
        }
        Func<Vector3, Vector3, Vector3> MovePosFunc = (pos1, pos2) => { //pos1からpos2方向へのベクトル
            float difference_x = pos2.x - pos1.x; //プレイヤーボール間の差x
            float difference_z = pos2.z - pos1.z; //プレイヤーボール間の差z
            float forward_x = pos2.x + (difference_x * weight); //ボール移動先X
            float forward_z = pos2.z + (difference_z * weight); //ボール移動先Z
            Debug.Log("difference_x:" + difference_x + " difference_z:" + difference_z);
            return new Vector3(forward_x, 0, forward_z);
        };
        Vector3 forward = MovePosFunc(hitPiecePos, ballPos);
        if (isReflectBall) {
            Vector3 reflect = MovePosFunc(ballPos, hitPiecePos);
            Vector3 reflectionPos = new Vector3(reflect.x, hitPiecePos.y, reflect.z); //コマ反射先
//            iTween.MoveTo(collision.gameObject, iTween.Hash("position", reflectionPos,
//                "time", Global.PieceTime, "oncomplete", "StopPiece", "oncompletetarget", this.gameObject)); //コマ移動
            forward.x += UnityEngine.Random.Range(-0.2f, 0.2f);
            forward.z += UnityEngine.Random.Range(-0.1f, 0.1f); //-0.1f, 0.1f
        }
        Vector3 ballMovePos = new Vector3(forward.x, Global.BallPosY, forward.z); //ボール移動先
        
        Debug.Log("ボール移動開始 (あなたのコマとボールが衝突) x:" + forward.x + " z:" + forward.z);
        BallMove(ballMovePos, 0, isReflectBall);
    }

    //プレイヤー攻撃時のボールの移動・停止処理
    void BallMove(Vector3 movePos, float count, bool isReflectBall) {
        if (Global.nowBallTweenActive == false) {
            Debug.Log("BallProcess BallMove コマ接触によるボール移動処理開始 movePos:"+movePos);
            iTween.MoveTo(transform.gameObject, iTween.Hash("position", movePos, "time", Global.BallTime,
                "oncomplete", "StopBall", "oncompletetarget", this.gameObject)); //パネル衝突時のボールの跳ね返り処理
            float ballRoll = Global.BallRoll(pieceEndToBall);
            if (isReflectBall) ballRoll = Global.BallReflectionRoll;
            iTween.RotateTo(transform.gameObject, iTween.Hash("x", ballRoll, "time", Global.BallTime)); //回転
            Global.nowBallTweenActive = true;
        }
    }

    // Global.BallTime秒で指定した位置にアニメーションする
    IEnumerator BallMoveTime(Vector3 movePos, float count, bool isReflectBall) {
        iTween.MoveTo(transform.gameObject, iTween.Hash("position", movePos, "time", Global.BallTime,
            "oncomplete", "StopBall", "oncompletetarget", this.gameObject)); //ボール移動
        float ballRoll = Global.BallRoll(pieceEndToBall);
        if (isReflectBall) ballRoll = Global.BallReflectionRoll;
        iTween.RotateTo(transform.gameObject, iTween.Hash("x", ballRoll, "time", Global.BallTime)); //回転
        //yield return new WaitForSeconds(Global.PieceTime - count);
        yield return null; //意味なし
    }

    //ボールのiTween終了時の処理
    void StopBall() {
        Debug.Log("BallProcess ボールのTween終了");
        Global.nowBallTweenActive = false;
        Global.nowMoveBall = false;
        transform.rigidbody.isKinematic = false;
        transform.rigidbody.velocity = Vector3.zero;
        transform.rigidbody.angularVelocity = Vector3.zero;

    }


}
