﻿using UnityEngine;
using System;
using System.Collections;

public class RotateSlotPanel : MonoBehaviour {


	bool isPreParePanelStart = false;
	float allNumber = 10f;
	float speed;

	public int number;
	float nDegree = 0.0f; // 度数値(0~360)
	float StartSpeed = 4.5f; //オブジェクトの初期スピード 2.5f
	float nSpeed; // オブジェクトの現在のスピード
    float addSpeed = 0.07f; //オブジェクトの1フレームごとの加速度 0.028f
	float nMaxSpeed = 22f; // オブジェクトの最終的スピード 18f
	float nDegreeToRadian = Mathf.PI / 180.0f; // nDegreeの値をラジアンに変換する
	float nRadian; // nDegreeの値をラジアンに変換した値
	float nCenterX = 0f; // 楕円運動の水平中心座標
	float nCenterZ = -6.0f; // 楕円運動の前後中心座標 -6.1f
	float nRadiusX = 1.4f; // 楕円運動の水平方向の半径 1.6f
	float nRadiusZ = 0.4f; // 楕円運動の前後方向の半径 0.5f

	void Start () {
		nSpeed = StartSpeed;
		nDegree = (360f / allNumber) * number;
	}
	
	void Update () {
		if (Global.isStopPanel) {
			return;
		} 
		if (nSpeed < nMaxSpeed) {
			nSpeed += addSpeed; 
		}
		nDegree -= nSpeed; // 移動する分の値をnDegreeに入れる
		nDegree = (nDegree % 360 + 360) % 360; // nDegreeの値を0から360までの間の数値に変換する
		nRadian = nDegree * nDegreeToRadian; // nDegreeの値をラジアンに変換した値をnRadianに入れる
		
		transform.position = 
			new Vector3(nCenterX + Mathf.Cos(nRadian) * nRadiusX, 0.33f, nCenterZ + Mathf.Sin(nRadian) * nRadiusZ);
	}

	public void ResetStatus() {
		//Debug.Log(" No."+number + " : ResetStatus start");
		nSpeed = StartSpeed;
		nDegree = (360f / allNumber) * number;
	}

    //このパネルが当たったときの得点処理
    public void HitScore() {

        if(Global.gameCount == Global.GameCount.First) {
            //Global.firstScore = number;
            Global.gameCount = Global.GameCount.FirstStop;
            Global.sumScore = number;
            SetBlueScore(number, 1);
        } else if(Global.gameCount == Global.GameCount.Second) {
            //Global.secondScore = number;
            Global.gameCount = Global.GameCount.SecondStop;
            Global.sumScore += number;
            SetBlueScore(number, 2);
        } else if(Global.gameCount == Global.GameCount.Third) {
            //Global.thirdScore = number;
            Global.gameCount = Global.GameCount.End;
            Global.sumScore += number;
            SetBlueScore(number, 3);
        } else {
            Debug.Log("パネルへの衝突は有効ではありません gameCount:"+ Global.gameCount);
        }
    }

    //青スコアも黄スコアも反映
    void SetBlueScore(int number, int gameStep) {
        bool isTen = number > 9 ? true : false;
        int oneNumber = isTen ? 0 : number;
        String tenNumber = isTen ? "1" : "0"; 
        //一桁目
        Global.SpriteImageSet(
            "soccerMinigame/blueScore/0" + oneNumber, //使用画像パス
            "Canvas/"+ gameStep + "ScoreOne"); //画像変更対象オブジェクトパス
        //2桁目
        Global.SpriteImageSet(
            "soccerMinigame/blueScore/0" + tenNumber, //使用画像パス
            "Canvas/" + gameStep + "ScoreTen"); //画像変更対象オブジェクトパス

        int totalOneNumber = Global.sumScore % 10;
        int totalTenNumberInt = Global.sumScore / 10;
        String totalTenNumber = (number > 0) ? totalTenNumberInt.ToString() : "alpha";
        //合計1桁目
        Global.SpriteImageSet(
            "soccerMinigame/yellowScore/0" + totalOneNumber, //使用画像パス
            "Canvas/TotalScoreOne"); //画像変更対象オブジェクトパス
        //合計2桁目
        Global.SpriteImageSet(
            "soccerMinigame/yellowScore/0" + totalTenNumber, //使用画像パス
            "Canvas/TotalScoreTen"); //画像変更対象オブジェクトパス
    }

}
