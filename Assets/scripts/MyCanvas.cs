﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// 現在未使用(Don't use now)
public class MyCanvas : MonoBehaviour {

    public Canvas canvas; //canvasコンポーネントをセットしておく

    void Start() {
        Debug.Log("キャンバス取得 canvas:"+canvas);
    }

    /// 表示・非表示を設定する。Canvasの子要素のみたどり、孫要素はたどっていない
    public void SetActive(string name, bool isActive) {
        //Debug.Log("SetActive 取得canvas:" + canvas);
        foreach (Transform child in canvas.transform) {
            //Debug.Log("child name:" + child.name);
            if (child.name == name) { // 指定した名前と一致
                child.gameObject.SetActive(isActive); // 表示フラグを設定
                //Debug.Log("UI表示成功 name:" + name + " child:" + child +" bool:"+b);
                return;
            }
        }
        Debug.LogWarning("Not found objname:" + name); // 指定したオブジェクト名が見つからなかった
    }

    /// 表示・非表示を設定する。Canvasの子要素のみたどり、孫要素はたどっていない。ボタンにのみ使用
    public void SetEnableBtn(string name, bool enable)
    {
        //Debug.Log("SetActive 取得canvas:" + canvas);
        foreach (Transform child in canvas.transform)
        {
            //Debug.Log("child name:" + child.name);
            if (child.name == name)
            { // 指定した名前と一致
                child.gameObject.GetComponent<Button>().enabled = enable; // 表示フラグを設定
                //Debug.Log("UI表示成功 name:" + name + " child:" + child +" bool:"+b);
                return;
            }
        }
        Debug.LogWarning("Not found objname:" + name); // 指定したオブジェクト名が見つからなかった
    }

    /// 表示・非表示を設定する。Canvasの孫要素をたどる(必要であれば、foreachと引数を増やすことで、ひ孫等要素をたどる拡張メソッドを作成)
    /// 最初に該当したもので検索をかけるため、後に同じものがある場合はそちらは引っかからない。
    public void SetActiveChild(string parentName, string name, bool isActive) {
        foreach (Transform child in canvas.transform) { // Canvasの子要素をたどる
            //Debug.Log("parent name:" + parentName+ " nowChild:" +child.name);
            if (child.name == parentName) {
                //Debug.Log("child2s:"+child.transform);
                foreach (Transform child2 in child.transform) { //CanvasのparentName子要素について、孫要素をたどる
                    //Debug.Log("child name:" + child.name);
                    if (child2.name == name) {
                        child2.gameObject.SetActive(isActive);
                        return;
                    }
                }
            }
        }
        // 指定したオブジェクト名が見つからなかった
        Debug.LogWarning("Not found obj parentName:"+parentName + " childName:" + name);
    }

}
