﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Net.Sockets; //ソケット通信用
using System.Text;
using System;

//このクラスはプロジェクト内の全クラスで使用される
public class Global : MonoBehaviour {

    //現在は数値は使用していないので削除可能
    public enum GameCount
    {
        First = 1,
        FirstStop,
        Second,
        SecondStop,
        Third,
        End,
        Next,
    }

    static public bool isGameStop; //抽選が無くなっている場合はtrue

    static public GameCount gameCount = GameCount.First;
    //抽選画面
	static public int firstScore;
	static public int secondScore;
	static public int thirdScore;
    static public int sumScore;
    static public int passcode; //設定画面に行くためのパスコード
    static public int TRUEPASS = 1955; //正しいパスコード
    //設定画面
    static public bool nowLotNoSet;
    static public bool nowLotAmountSet;
    static public int inputLotNo; //管理画面で入力された抽選No
    static public int inputLotAmount; //管理画面で入力された出現数
    static public int[] setLot = new int[21];
    static public int[] hitLot = new int[21];
    static public int allLot = 10; //ここで設定した等数まで使用
    static public bool useBigHitLimit = false; //1等の残り本数が1本のときに、全体の残りが減るまで1等が出ないようにする処理を行う場合はtrue
    static public float shootTempo = 1.7f; //1.7秒後に次のシュートが可能になるのが正規 
    static public int setLotAll;
    static public int hitLotAll;
    static public int nextHitLotNo; //抽選で決定したNo
    static public int nextHitScore; //抽選で決定したスコア

    static public float ballWaitTime;

    static public void ResetAll() {
        firstScore = 0;
        secondScore = 0;
        thirdScore = 0;
        sumScore = 0;
        nextHitLotNo = 0;
        nextHitScore = 0;
        gameCount = GameCount.First;
        Global.isSlideStart = false;
        Global.isSlideActionActive = true;
        Global.ballWaitTime = 0f;

     }

    static public void selectHitNo() {
        bool useZorome = false; //ゾロ目が1等の場合はtrue、ゾロ目を使用しない場合はfalse
        Debug.Log("シーン名：" + Application.loadedLevelName);
        if (Application.loadedLevelName != Global.Scenes.GameMain.ToString()) return;

        setLotAll = PlayerPrefs.GetInt("setLotAll", 0);
        hitLotAll = PlayerPrefs.GetInt("hitLotAll", 0);
        int remainAll = setLotAll - hitLotAll;
        Debug.Log("残り本数:" + remainAll + " 設定合計数:" + setLotAll + " 抽選済み合計数:" + hitLotAll);
        if (remainAll < 1) {
            Global.isGameStop = true;
            Global.isSlideActionActive = false;
            return;
        } else {
            Global.isGameStop = false;
        }
        int[] remainLot = new int[21];
        for (int i = 1; i < 11; i++) {
            remainLot[i] = Global.setLot[i] - Global.hitLot[i];
        }
        float selectRandom = UnityEngine.Random.Range(0f, (float)remainAll);

        /* ここから、1等が出にくくなる処理。
           具体的には、1等の本数が残り1本で、全体の残り本数が全体設定本数の1/(remainCheckRatio)以上なら1等は出さない */
        if (useBigHitLimit) {
            int remainCheckRatio = 5; //全体の残り本数が1/remainCheckRatioになるまでは1等は出ないようにする
            bool lotNo1RemainOne = Global.setLot[1] == 1 ? true : false; //1等が残り1本ならtrue
            bool remainLotValueNotLow = remainAll > (setLotAll / remainCheckRatio) ? true : false; //残り本数が設定合計本数の1/(remainCheckRatio)以上ならtrue
            while (true) {
                bool selectNo1 = (selectRandom <= remainLot[1]) ? true : false; //乱数で1等にヒットしたらtrue
                if (lotNo1RemainOne && remainLotValueNotLow && selectNo1) {
                    selectRandom = UnityEngine.Random.Range(0f, (float)remainAll);
                } else {
                    break;
                }
            }
        }
        /* 追加1等減少処理ここまで */


        nextHitLotNo = -1;
        float remainCount = 0;
        for (int i = 1; i < 11; i++) {
            remainCount += remainLot[i];
            if (selectRandom <= remainCount) {
                nextHitLotNo = i;
                break;
            }
        }
        Debug.Log("残り本数:" + remainAll + " 乱数:" + selectRandom + " remainCount:" + remainCount);
        if (useZorome) { decideLotUseZorome(); } else if (useZorome == false) { decideLot(); }
        Debug.Log("最終得点:" + Global.nextHitScore);

        int judge1, judge2, judge3;
        //ゾロ目の場合の処理
        if ((nextHitLotNo == 1) && (nextHitScore % 3 == 0) && useZorome) { //nextHitLotNo==1(1等)ならゾロ目も可能
            int zorome = nextHitScore / 3;
            judge1 = zorome; judge2 = zorome; judge3 = zorome;
            Debug.Log("ゾロ目発生");
        } else {
            //ゾロ目でない場合(ゾロ目を判定しない場合の処理も含む)
            bool isZoromeRisk = (nextHitScore % 3 == 0) ? true : false; //ゾロ目に注意
            int zoromeNumber = nextHitScore / 3; //ゾロ目の可能性がある場合のゾロ目値
            judge1 = 0;
            if (Global.nextHitScore < 12) { //合計値12未満で、取れる値が10より小さい
                judge1 = UnityEngine.Random.Range(1, nextHitScore - 1); //MaxがnextHitScore-1のRangeなので、nextHitScore-2の値までしか出ない
            } else {
                if (nextHitScore >= 22) {
                    judge1 = UnityEngine.Random.Range(nextHitScore - 20, 10);
                } else {
                    judge1 = UnityEngine.Random.Range(1, 11);
                }
            }
            int remainScore = nextHitScore - judge1;
            judge2 = 0;
            if (remainScore < 11) { //残りの合計値が11未満で、取れる値が10より小さい
                judge2 = UnityEngine.Random.Range(1, remainScore);
                if (useZorome) {
                    while (isZoromeRisk && zoromeNumber == judge1 && zoromeNumber == judge2) {
                        judge2 = UnityEngine.Random.Range(1, remainScore); //MaxがremainScoreのintのRangeなので、remainScore-1の値までしか出ない
                    }
                }
            } else {
                if (remainScore > 10 + judge2) { //例：17 > 10 +judge2(1~6)
                    judge2 = UnityEngine.Random.Range(remainScore - 10, 10);
                    if (useZorome) {
                        while (isZoromeRisk && zoromeNumber == judge1 && zoromeNumber == judge2) {
                            judge2 = UnityEngine.Random.Range(remainScore - 10, 10);
                        }
                    }
                } else { //judge2=0なのでこの場所には行かない
                    judge2 = UnityEngine.Random.Range(1, 11);
                    if (useZorome)  {
                        while (isZoromeRisk && zoromeNumber == judge1 && zoromeNumber == judge2) {
                            judge2 = UnityEngine.Random.Range(1, 11);
                        }
                    }
                }
            }
            judge3 = remainScore - judge2;
        }

        Debug.Log("j1:" + judge1 + " j2:" + judge2 + " j3:" + judge3);
        int firstSet = UnityEngine.Random.Range(0, 3);
        int secondSet = UnityEngine.Random.Range(0, 2);
        if (firstSet == 0) {
            firstScore = judge1;
            if (secondSet == 0) {
                secondScore = judge2; thirdScore = judge3;
            } else {
                secondScore = judge3; thirdScore = judge2;
            }
        } else if (firstSet == 1) {
            secondScore = judge1;
            if (secondSet == 0) {
                firstScore = judge2; thirdScore = judge3;
            } else {
                firstScore = judge3; thirdScore = judge2;
            }
        } else {
            thirdScore = judge1;
            if (secondSet == 0) {
                firstScore = judge2; secondScore = judge3;
            } else {
                firstScore = judge3; secondScore = judge2;
            }
        }
    }

    //当選する等に対応した合計スコアを決定する。ゾロ目での1等判定が無いバージョン(10等版)
    static public void decideLot() {
        //ゾロ目になる確率は本来は高くないが、どの値も均等に出るようにしている
        switch (nextHitLotNo) {
            case 1: //28-30
                int r = UnityEngine.Random.Range(0, 3); //intでは(min, max)でmin~max-1の範囲の値を返す
                nextHitScore = 28 + r;
                break;
            case 2: //25-27
                int r2 = UnityEngine.Random.Range(0, 3);
                nextHitScore = 25 + r2;
                break;
            case 3: //22-24
                int r3 = UnityEngine.Random.Range(0, 3);
                nextHitScore = 22 + r3;
                break;
            case 4: //19-21
                int r4 = UnityEngine.Random.Range(0, 3);
                nextHitScore = 19 + r4;
                break;
            case 5: //16-18
                int r5 = UnityEngine.Random.Range(0, 3);
                nextHitScore = 16 + r5;
                break;
            case 6: //13-15
                int r6 = UnityEngine.Random.Range(0, 3);
                nextHitScore = 13 + r6;
                break;
            case 7: //10-12
                int r7 = UnityEngine.Random.Range(0, 3);
                nextHitScore = 10 + r7;
                break;
            case 8: //7-9
                int r8 = UnityEngine.Random.Range(0, 3);
                nextHitScore = 7 + r8;
                break;
            case 9: //4-6
                int r9 = UnityEngine.Random.Range(0, 3);
                nextHitScore = 4 + r9;
                break;
            case 10: //3
                nextHitScore = 3; //ランダム性なし
                break;
            default:
                Debug.Log("============ 抽選失敗!! ============");
                break;
        }
    }

    //当選する等に対応した合計スコアを決定する。ゾロ目での1等判定が無いバージョン(全7等版、また使う可能性があるのでコメントアウトして残す)
    //static public void decideLot()
    //{
    //    //ゾロ目になる確率は本来は高くないが、どの値も均等に出るようにしている
    //    switch (nextHitLotNo)
    //    {
    //        case 1: //27-30
    //            int r = UnityEngine.Random.Range(0, 4); //intでは(min, max)でmin~max-1の範囲の値を返す
    //            nextHitScore = 27 + r; //27-30
    //            break;
    //        case 2: //23-26
    //            int r2 = UnityEngine.Random.Range(0, 4);
    //            nextHitScore = 23 + r2; //23-26
    //            break;
    //        case 3: //19-22
    //            int r3 = UnityEngine.Random.Range(0, 4);
    //            nextHitScore = 19 + r3;
    //            break;
    //        case 4: //15-18
    //            int r4 = UnityEngine.Random.Range(0, 4);
    //            nextHitScore = 15 + r4;
    //            break;
    //        case 5: //11-14
    //            int r5 = UnityEngine.Random.Range(0, 4);
    //            nextHitScore = 11 + r5;
    //            break;
    //        case 6: //7-10
    //            int r6 = UnityEngine.Random.Range(0, 4);
    //            nextHitScore = 7 + r6;
    //            break;
    //        case 7: //3-6
    //            int lastNo = UnityEngine.Random.Range(0, 16);
    //            //7等は多く、全部1のゾロ目(合計3)は目立ちそうなので、3の出る確率を下げておく(7等において1/16)
    //            if (lastNo == 0) nextHitScore = 3;
    //            if (1 <= lastNo && lastNo <= 3) nextHitScore = 4; //(1,1,2)の組み合わせなので極力避ける
    //            if (4 <= lastNo && lastNo <= 7) nextHitScore = 5; //(1,1,3),(1,2,2)の組み合わせ。まあ少しはましか。ただ露骨な低さは出てしまう
    //            if (8 <= lastNo && lastNo <= 15) nextHitScore = 6; //パターン多め(1,1,4や2,1,3など)なので、こちらを多く出す
    //            break;
    //        default:
    //            Debug.Log("============ 抽選失敗!! ============");
    //            break;
    //    }
    //}

    //当選する等に対応した合計スコアを決定する。ゾロ目での1等判定をするバージョン
    static public void decideLotUseZorome () {
        switch (nextHitLotNo) {
            case 1: //ゾロ目と26-30
                int r = UnityEngine.Random.Range(0, 19); //intでは(min, max)でmin~max-1の範囲の値を返す
                //以下、3回でゾロ目になる本来の確率は1/100なので、ゾロ目の確率をやや下げている。
                if(r <= 8) nextHitScore = (r + 1) * 3; //3,6,9,12,15,18,21,27
                if (9 <= r && r <= 11) nextHitScore = 28;
                if (12 <= r && r <= 14) nextHitScore = 29;
                if (r == 15) nextHitScore = 30;
                if (16 <= r && r <= 18) nextHitScore = 26;
                break;
            case 2: //21-25
                int r2 = UnityEngine.Random.Range(0, 5);
                nextHitScore = 21 + r2; //21-25
                break;
            case 3: //16-20
                int r3 = UnityEngine.Random.Range(0, 5);
                nextHitScore = 16 + r3; //16-20
                break;
            case 4: //7-15
                int r4 = UnityEngine.Random.Range(0, 9);
                nextHitScore = 7 + r4; //7-15
                break;
            case 5: //4-6
                int r5 = UnityEngine.Random.Range(0, 3);
                nextHitScore = 4 + r5; //4-6
                break;
            default:
                Debug.Log("============ 抽選失敗!! ============");
                break;
        }
    }

    //当選設定確認 
    static public void SetLotNoAndAmountText() {
        if (GameObject.Find("LotNowNumber/Text") == null) return;
        setLotAll = 0;
        hitLotAll = 0;
        //設定ロトカウント
        for (int i = 1; i < 11; i++) {
            Global.setLot[i] = PlayerPrefs.GetInt("setLot" + i, 0); //第二引数は値が無い場合に返す値
            setLotAll += Global.setLot[i];
        }
        //抽選済みロトカウント
        for (int i = 1; i < 11; i++) {
            Global.hitLot[i] = PlayerPrefs.GetInt("hitLot" + i, 0); //第二引数は値が無い場合に返す値
            hitLotAll += Global.hitLot[i];
        }
        PlayerPrefs.SetInt("setLotAll", setLotAll);
        PlayerPrefs.SetInt("hitLotAll", hitLotAll);
        String text1 = "当選設定確認 　抽選本数合計:" + setLotAll + "本\n現在の利用回数:"+ hitLotAll + "回";
        GameObject.Find("LotSum/Text").GetComponent<Text>().text = text1;
        //String text2 = "■1等(27~30) 設定" + Global.setLot[1] + "本 出現数" + Global.hitLot[1] + "本     ■2等(23~26) 設定" + Global.setLot[2] + "本 出現数" + Global.hitLot[2] + "本\n" +
            //"■3等(19~22) 設定" + Global.setLot[3] + "本 出現数" + Global.hitLot[3] + "本    ■4等(15~18) 設定" + Global.setLot[4] + "本 出現数" + Global.hitLot[4] + "本\n" +
            //"■5等(11~14) 設定" + Global.setLot[5] + "本 出現数" + Global.hitLot[5] + "本    ■6等(7~10) 設定" + Global.setLot[6] + "本 出現数" + Global.hitLot[6] + "本\n" +
            //"■7等(6点以下) 設定" + Global.setLot[7] + "本 出現数" + Global.hitLot[7];
        String text2 = "■1等(28~30) 設定" + Global.setLot[1] + "本 出現数" + Global.hitLot[1] + "本     ■2等(25~27) 設定" + Global.setLot[2] + "本 出現数" + Global.hitLot[2] + "本\n" +
            "■3等(22~24) 設定" + Global.setLot[3] + "本 出現数" + Global.hitLot[3] + "本    ■4等(19~21) 設定" + Global.setLot[4] + "本 出現数" + Global.hitLot[4] + "本\n" +
            "■5等(16~18) 設定" + Global.setLot[5] + "本 出現数" + Global.hitLot[5] + "本    ■6等(13~15) 設定" + Global.setLot[6] + "本 出現数" + Global.hitLot[6] + "本\n" +
            "■7等(10~12) 設定" + Global.setLot[7] + "本 出現数" + Global.hitLot[7] + "本    ■8等(7~9) 設定" + Global.setLot[8] + "本 出現数" + Global.hitLot[8] + "本\n" +
            "■9等(4~6) 設定" + Global.setLot[9] + "本 出現数" + Global.hitLot[9] + "本    ■10等(3) 設定" + Global.setLot[10] + "本 出現数" + Global.hitLot[10] + "本\n";
        GameObject.Find("LotNowNumber/Text").GetComponent<Text>().text = text2;
    }

    /* Below code is no relate to this app. Some day, this program will delete. */

    //攻撃サイドがどちらかを管理
    public enum Side : int {
        No = 0,
        My = 1,
        Opponent = 2,
    }

    //Unityのシーン名称を管理
    public enum Scenes {
        GameStart,
        GameTeamSelect,
        GameMypage,
        GameRoomInput,
        GameRule,
        GameMain,
        GameSetting,
    }

    //試合中に、試合の状況を管理(現在は通常とファールのみ)
    public enum GameStep {
        Normal,
        FoulMe,
        FoulOpponent
    }

    //ファールステップならtrue
    public static bool NowFaulStep() {
        if (Global.step == Global.GameStep.FoulMe || Global.step == Global.GameStep.FoulOpponent) {
            return true;
        } else {
            return false;
        }
    }

    /*ユーザデータ*/
	static public bool isStopPanel = false; //SlotPanel stop
	static public GameObject[] slotPanels;
	static public bool isSlideActionActive = true;
	static public bool isSlideStart;
	static public bool isStartNextGame = false; //Next Game Start


    static public string userName; //ゲーム上のユーザ名
    static public string uuid; //アプリをアンインストールするまで不変でかつ一意のUUID
    static public string mail = ""; //アカウント作成時の設定メールアドレス
    static public long maxEnergy; //ゲーム体力の最大値(変動する可能性あり)
    static public long gameEnergy; //ゲーム体力(現在はゲームをプレイできる残り回数)
    static public float nextEnergyTime; //次にゲーム体力が回復するまでの時間(秒) -99999のときは体力全快
    static public long premierNum; //所持プレミアムアイテム数
    static public string nowLeague; //現在の所属リーグ
    static public long gameScore; //現在のゲームスコア
    static public long nextNeedScore; //次のリーグへの必要スコア

    static public bool isDeerman; //ディアーマンを表示するときはtrue(奈良クラブ版)

    /* 試合以外の管理 */
    static public bool isTutorial; //チュートリアル中はtrue
    static public bool isNoTouchAction; //画面をタッチしても反応を無くす場合(ポップアップ出現時のボタンなど)true
    static public bool isCreateUser; //ユーザアカウント作成プロセス中はtrue
    static public int nowTabNo = -1; //ユーザが現在選択しているタブ。未選択時は0、TOP画面では-1

    static public int selectMyTeamNo = -2; //選択した自分のチームNo(国No)
    static public int selectOpponentTeamNo = -2; //選択した相手のチームNo(国No)

    /* ゲーム進行関連処理 */
    static public GameStep step = GameStep.Normal; //試合の状況
    static public float runTime = 0; //経過時間
    static public bool nowGameSetup = false; //試合開始前、選手コマのセットアップ中はtrue
    static public Side firstGameSide = Side.No; //試合開始時の攻撃側を格納 
    static public Side nextGameSide = Side.No; //どちらかの得点時に、次のキックオフサイドを記録。それ以外のときはNO
    static public bool isPrepareKickoff = false; //キックオフの準備中(選手配置時)はtrue
    static public int playerScore = 0; //プレイヤーの試合得点
    static public int opponentScore = 0; //CPUの試合得点
    static public bool isGameActive = false; //ゲーム停止中以外はtrue
    static public Side ballLastHitSide = Side.No; //Myなら味方側が最後にボールに接触、OPPONENTなら敵側が最後にボールに接触
    static public int cpuLevel = 3; //CPUの強さレベル 数値が大きい程強い。デフォルト値を3にしている
    static public bool isMyTurnMoveOneOver = false; //新しく攻撃ターンになった後に1度以上移動したらtrue(local適用。ターンが交代する度falseにする)
    static public float noTouchTime; //攻撃側が移動しないまま経過している時間
    static public bool nowGoal = false; //得点発生直後はtrue
    static public bool nowGameEnd = false; //試合終了時にはtrue

    /* 攻撃サイド */
    static public string ownerSideUserName; //オーナー側ユーザー名
    static public string clientSideUserName; //クライアント側ユーザー名
    static public Side gameSide = Side.No; //プレイヤーが攻撃側なら1、相手が攻撃側なら2、攻撃側未定時は0
    static public bool NowMySide() {
        if (gameSide == Side.My) { return true; } else { return false; }
    }
    static public bool NowOpponentSide() {
        if (gameSide == Side.Opponent) { return true; } else { return false; }
    }
    static public Side NowSideReverse() {
        if (NowMySide()) { return Side.Opponent; } else if (NowOpponentSide()) { return Side.My; } else { return Side.No; }
    }
    static public Side OwnerSide() { //自分がオーナーなら自分を攻撃側、そうでないなら相手を攻撃側に置く
        if (Global.IsMeOwner()) { return Side.My; } else { return Side.Opponent; }
    }

    /* ボールのフィールド外処理 */
    static public Side throwinSide = Side.No; //スローイン処理時以外はNO
    static public float throwinLineZ = -100; //スローイン発生時にボールが通過したラインZ位置(発生時以外は-100とする)。オンラインではOwnerのみ使用
    static public Side goalkickSide = Side.No;//ゴールキック処理の状況 
    static public Side cornerkickSide = Side.No;//コーナーキック処理の状況
    static public int cornerkickPoint = 0; //平常時は0,コーナーキックの際はそれぞれの角で1-4((オーナー視点で、1は敵側左、2は敵側右、3は味方側左、4は味方側右)
    static public int goalLineOutPoint = 0; //平常時は0、ボールがゴールラインを割った際は1-4(オーナー視点で、1は敵側左、2は敵側右、3は味方側左、4は味方側右)
    static public bool isFirstReturnKick = false; //フィールド外から復帰した最初のショット(スローイン、コーナーキック時に使用)

    /* ボール・コマ移動関連 */
    static public bool isFoulStartNow; //ファール処理開始時
    static public bool isFoulTouchBall; //ボール接触ファールならtrue
    static public Vector3 beforePiecePos; //守備側コマ移動前の位置(ファール時に戻る)
    static public int beforePieceNo; //守備側で移動したコマのNo
    static public Vector3 beforeBallPos; //ボールのファール時直前位置(ファール時にはボールを移動させた上で戻す)

    static public bool cpuBeforeSelectPiece = false; //CPU守備時に移動コマが決まればtrue
    static public bool nowMoveBall = false; //ボール移動中はtrue
    static public bool nowMoveMe = false; //プレイヤーコマ移動中はtrue
    static public bool nowMoveOpponent = false; //相手側が移動中はtrue

    static public int selectMyNo = -1; //タッチしたプレーヤーコマの番号(0-10)を保存 -1の時は該当なし 
    static public int selectVsNo = -1; //選択中の対戦相手コマ(オンライン対戦でも使用する)
    static public bool canDefenseMove = false; //守備側として動作可能なタイミングではtrue(攻撃側移動開始時より、1度移動可能)
    static public bool nowGoalKick = false; //プレーヤーのゴールキックになったときにtrue
    static public bool nowPlayerHitWall = false; //プレイヤーコマが壁やゴールポストに衝突したらtrue
    static public Vector3 touchLinePos = new Vector3(0, 0, 0); //コマがライン外に出た際の座標
    static public bool nowLineOut = false; //コマがライン外に出た際にtrue

    /* iTween動作 */
    static public bool nowPlayerTweenActive = false; //プレイヤーコマ動作のiTweenがアクティブならtrue
    static public bool nowCpuTweenActive = false; //CPUコマ動作のiTweenがアクティブならtrue
    static public bool nowBallTweenActive = false; //ボール動作のiTweenがアクティブならtrue

    /* ネットワーク関連 */
    static public bool isWaitVsPlayer; //オンライン対戦で、対戦相手を待っている間はtrue
    static public int[] myViewIds = new int[11]; //自コマのviewIdリスト
    static public bool isPlayOnlineGame = false; //オンライン対戦を始める場合はtrue
    static public bool isSetupOnlineGame = false; //オンライン対戦において、対戦開始前はtrue
    static public bool isRoomOwner = false; //自分がルームのオーナーであればtrue
    static public bool isFriendBattle = false; //フレンド対戦ならtrue
    static public int roomNum = -1; //入力されたルーム番号。未入力時やリセット時は-1
    static public bool nowOpponentDisconnect = false; //相手の接続が切断された際はtrue
    static public string MyPieceTag() { //自分の操作するコマのタグ名
        if (isPlayOnlineGame == false || isRoomOwner) { return OwnerPieceTag; } else { return ClientPieceTag; }
    }
    static public string VsPieceTag() { //対戦相手の操作するコマのタグ名
        if (isPlayOnlineGame == false || isRoomOwner) { return ClientPieceTag; } else { return OwnerPieceTag; }
    }
    //対象タグがコマタグかどうかを見る
    static public bool IsPieceTag(string tag) {
        if (tag == OwnerPieceTag || tag == ClientPieceTag) { return true; } else { return false; }
    }

    //対象タグが現在攻撃中のコマならtrue、それ以外のコマやオブジェクトならfalse
    static public bool IsOffenseTag(string tag) {
        if ((tag == MyPieceTag() && gameSide == Side.My) || (tag == VsPieceTag() && gameSide == Side.Opponent)) {
            return true;
        } else {
            return false;
        }
    }

    //対象タグが現在守備側のコマならtrue、それ以外のコマやオブジェクトならfalse
    static public bool IsDefenseTag(string tag) {
        if (IsPieceTag(tag) && IsOffenseTag(tag) == false) {
            return true;
        } else return false;
    }

    //自コマのうち、動作中コマの名前
    static public string MyMovePieceName() {
        if (MyPiece(Global.selectMyNo) == null) return "";
        return Global.MyPieceNameBase + Global.selectMyNo;
    }

    //相手コマのうち、動作中コマの名前
    static public string OpponentMovePieceName() {
        if (Global.selectVsNo < 0) {
            return "";
        }
        return Global.VsPieceTag() + Global.selectVsNo;
    }

    static public bool IsMeOwner() { //CPU戦の場合も含め,オーナー側ユーザである場合はtrue
        if (isPlayOnlineGame == false || isRoomOwner) { return true; } else { return false; }
    }
    static public bool IsMeClient() { //クライアント側ユーザである場合はtrue
        return !(IsMeOwner());
    }
    static public string SideName(bool isOwner, bool isPlayer) { //両サイドのユーザー名を取得
        if (isPlayer) {
            return Global.userName;
        } else { //CPUの場合
            if (isOwner) { return "HOME"; } else { return "AWAY"; }
        }
    }

    /* 固定値 */
    public const bool IsStamina = false; //選手体力の有無

    public const string MyPieceNameBase = "myPiece"; //ゲームの操作ユーザー側コマの名前(数字除く) クライアント側も同じ名前とする
    public const string OwnerPieceTag = "ownerPiece";
    public const string ClientPieceTag = "clientPiece";
    public const float ReflectWeight = 1.8f; //ボール・コマ反射の大きさ 0.4にしている
    public const float NormalPiecePosY = 0.35f; //コマのY軸高さ 0.3→0.35にして浮かしている(Physics対策)
    public const float DeermanPiecePosY = 0.4f; //ディアマンコマのY軸高さ
    public const float GKPiecePosZ = 5.6f; //GKコマのデフォルトZ位置(絶対値)
    public const float PieceTime = 1.8f; //コマの移動時間or速度 時間の場合は1.3fにしていた
    public const string TimeSpeed = "speed"; //iTweenで、timeかspeedか 両者は同時に使用できない
    public const string EaseType = "easeOutQuint"; //コマの移動速度の変化タイプ
    public const float BallPosY = 0.03f; //ボールの高さ
    public const float BallTime = 1.0f; //ボールの移動時間 1.5f
    public const float lineY = 0.001f; //ショットラインのy軸高さ 0.07f
    public const float BallPieceLimit = 0.7f; //攻撃時の、ボールと移動コマの距離限界(この値以内に隣接しているときは距離を離す)
    public const float BallPieceDiffCut = 10f; //ボールと攻撃コマの距離をいくつで割った分だけ離すか
    public const float BallReflectionRoll = 90f; //ボールとコマが攻撃以外で衝突した際のボールの回転角度

    public const float fieldEndX = 3.9f; //フィールドX軸境界値(絶対値) 以前は3.95f
    public const float fieldEndZ = 5.85f; //フィールドZ軸境界値(絶対値) 以前は5.9f
    public const float GoalEndX = 1.1f; //ゴールの左右の端X座標
    public const float BallThrowinX = 3.88f; //ボールのスローイン時のX軸座標(絶対値)
    public const float BallCornerX = Global.fieldEndX - 0.1f; //ボールコーナーキック時のX座標(絶対値)
    public const float BallCornerZ = Global.fieldEndZ - 0.15f; //ボールコーナーキック時のZ座標(絶対値)
    public const float PenaltyAreaX = 2.5f; //ペナルティエリアX軸境界値(絶対値)
    public const float PenaltyAreaZ = 4.6f; //ペナルティエリアZ軸境界値(絶対値)
    public const float ThrowinNearLength = 1.5f; //スローイン時に近づける限界値
    public const float CornerKickNearLength = 3f; //コーナーキック時に近づける限界値
    static public readonly Color TouchPieceColor = Color.cyan; //選手コマをタッチした際の色変化
    public const float NoTouchLimit = 10f; //攻撃側が移動しないままこの秒数が経過すると攻撃側が交代する

    public const float NextTimeAsMaxEnergy = -99999; //体力全回復時の次回回復までの秒数
    public const int UserNameLength = 8; //ユーザ名の最大長
    public const int MailLength = 256; //IETF RFC 2821 SMTPでのメールアドレスの最大長
    public const int PasswordLength = 20; //ログインパスワードの最大長さ
    public const int AuthCodeLength = 1000; //認証コードの制限はゆるくしておく


    //それぞれ0-10のコマのデフォルト位置
    static public readonly float[] OwnerDefaultPosX = { 0, -2, 0, 2, -2.5f, -1, 1, 2.5f, -2, 0, 2 };
    static public readonly float[] OwnerDefaultPosZ = { 
        -GKPiecePosZ, -3.5f, -3.5f, -3.5f, -2.5f, -2.5f, -2.5f, -2.5f, -1.5f, -1.5f, -1.5f};
    static public readonly float[] ClientDefaultPosX = { 0, -2, 0, 2, -2.5f, -1, 1, 2.5f, -2, 0, 2 };
    static public readonly float[] ClientDefaultPosZ = { 
        GKPiecePosZ, 3.5f, 3.5f, 3.5f, 2.5f, 2.5f, 2.5f, 2.5f, 1.5f, 1.5f, 1.5f};

    public static bool isParameter = false; //選手コマにパラメータがある場合はtrue
    public static int gameType = 3; //1なら奈良、2ならスペランツァ、3ならチーム指定なしの一般アプリ

    public static string NaraNetworkVersion = "v1.1-naraClub";
    public static string SperanzaNetworkVersion = "v1.1-speranza";

    public static string FlickSoccerGameVer = "1.3"; //現在、v1.2.0系は1.1にしている→v1.3.0は1.3にしている
    public static string FlickSoccerVersionStr = "v" + FlickSoccerGameVer + "-flickSoccer";
    public static string GetNetworkVersion() {
        if (gameType == 1) {
            return Global.NaraNetworkVersion;
        } else if (gameType == 2) {
            return Global.SperanzaNetworkVersion;
        } else {
            return Global.FlickSoccerVersionStr;
        }
    }

    //タイトル画面
    public static string TopView() {
        if (gameType == 1) {
            return "NaraClubTopView";
        } else if (gameType == 2) {
            return "SperanzaTopView";
        } else {
            return "GeneralTopView";
        }
    }

    //サーバ通信を含むアプリかどうか
    public static bool IsContainServerAccess() {
        if (gameType != 1) {
            return true;
        } else {
            return false;
        }
    }

    //言語設定が日本語かどうか判定
    public static bool IsLangJapan() {
        //if (Application.systemLanguage != SystemLanguage.Japanese) { //英語テスト用
        if (Application.systemLanguage == SystemLanguage.Japanese) { //正規
            return true;
        } else {
            return false;
        }
    }

    //端末上の設定言語に対応したテキストを返す
    public static string SetLangText(string japaneseText, string englishText) {
        if (IsLangJapan()) {
            return japaneseText;
        } else {
            return englishText;
        }
    }

    /// <summary>
    /// コマ種別に対応したY軸高さを取得
    /// </summary>
    /// <param name="isOwnerPiece"></param>
    /// <param name="pieceNo"></param>
    /// <returns></returns>
    public static float GetPiecePosY(bool isOwnerPiece, int pieceNo) {
        if (Global.isDeerman && (Global.IsMeOwner() == isOwnerPiece || Global.isPlayOnlineGame == false)) {
            //return DeermanPiecePosY;
            return NormalPiecePosY;
        } else {
            return NormalPiecePosY;
        }
    }

    //アプリの対象処理チーム
    public static string GameTargetTeam() {
        if (gameType == 1) {
            return "NaraClubPiece";
        } else if (gameType == 2) {
            return "SperanzaPiece";
        } else {
            return "NaraClubPiece";
        }
    }

    /// <summary>
    /// ユーザデータのローカルロード(アプリ内に記録されている情報のロード。これを元にサーバでチェックする)
    /// </summary>
    public static void SetUserData() {
        Global.userName = Global.B64Decode(PlayerPrefs.GetString("userName")); //ユーザ名のロード(日本語対策でB64デコード)
        Global.uuid = PlayerPrefs.GetString("uuid"); //UUIDのロード
    }

    /// <summary>
    /// ホーム画面（スタート画面orマイページ画面）に戻る
    /// </summary>
    public static void LoadBackHomeScene() {
        if (gameType == 1) {
            Application.LoadLevel(Global.Scenes.GameStart.ToString());
        } else {
            switch (Global.nowTabNo) {
                case -1:
                    Application.LoadLevel(Global.Scenes.GameStart.ToString());
                    break;
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                default:
                    Application.LoadLevel(Global.Scenes.GameMypage.ToString());
                    break;
            }
        }
    }

    public const string Local_Domain_Home = "http://192.168.0.11:9000"; //アクセスするローカルサーバのドメイン(Home)
    public const string Local_Domain = "http://192.168.1.8:9000"; //アクセスするローカルサーバのドメイン(office 有線)
    public const string Local_Domain2 = "http://192.168.1.7:9000"; //アクセスするローカルサーバのドメイン(office 無線)
    public const string Test_Domain = "http://test.sapoca.net"; //アクセスするテストサーバのドメイン。
    public const string Production_Domain = "http://sapoca.net"; //アクセスするサーバのドメイン。
    public static string GetDomain() { //使用サーバによって切り分け
        return Production_Domain;
    }

    public const string TopPath = "/soccer/";

    /* 固定オブジェクト */
    public static Transform ball; //オンライン時にも、ボールは各々で管理
    public static Transform mainCamera;

    static public GameObject[] owner = new GameObject[11];
    static public GameObject[] client = new GameObject[11]; //クライアントコマを処理するのはオーナーのみ
    static public Transform MyPiece(int no) { //自分が操作している側のコマ 0はGK、1-10が通常の選手
        if (no < 0) return null;
        if (Global.isPlayOnlineGame == false && (owner[no] == null || client[no] == null)) {
            return null;
        } else if (Global.IsMeOwner()) {
            if (owner[no] == null) { return null; } else { return owner[no].transform; }
        } else {
            if (client[no] == null) { return null; } else { return client[no].transform; }
        }
    }
    static public Transform VsPiece(int no) { //相手が操作している側のコマ(CPU用) オンライン時は使用できない
        if (no < 0 || owner[no] == null || client[no] == null) {
            return null;
        } else if (Global.IsMeOwner()) { return client[no].transform; } else { return owner[no].transform; }
    }

    void Awake() {
        Application.targetFrameRate = 60;
    }

    void Start() {
        Debug.Log("GlobalのStart起動");
        if(GameObject.FindGameObjectWithTag("ball") != null) {
            ball = GameObject.FindGameObjectWithTag("ball").transform;
        }
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    /// <summary>
    /// ディスプレイに対する相対位置でRectを取得。leftRateは左から10分の何か、topRateは上から10分の何か。
    /// </summary>
    /// <param name="leftRate">左から10分の何か</param>
    /// <param name="topRate">上から10分の何か</param>
    /// <param name="width">横幅px</param>
    /// <param name="height">高さpx</param>
    /// <returns></returns>
    static public Rect RelativeRect(float leftRate, float topRate, float width, float height) {
        float widthOneTenth = Screen.width / 10;
        float heightOneTenth = Screen.height / 10;
        return new Rect(widthOneTenth * leftRate, heightOneTenth * topRate, width, height);
    }

    /// <summary>
    /// それぞれのコマのタグより、オブジェクトをスクリプトから取得(CPU戦用)
    /// </summary>
    static public Transform[] SetPieceObjectsByTag(string tag) {
        Debug.Log("SetPieceObjectsByTag tag:" + tag);
        Transform[] pieces = new Transform[11];
        GameObject[] tagObjects = GameObject.FindGameObjectsWithTag(tag);
        foreach (GameObject piece in tagObjects) {
            for (int i = 0; i < 11; i++) {
                if ((tag == Global.OwnerPieceTag && piece.name == Global.MyPieceNameBase + i) ||
                    (tag == Global.ClientPieceTag && piece.name == tag + i)) {
                    pieces[i] = piece.transform;
                }
            }
        }
        return pieces;
    }

    /// <summary>
    ///  ターゲット位置に最も近い自コマのナンバーを返す
    /// </summary>
    /// <param name="targetPos"></param>
    /// <returns></returns>
    static public int NearOwnerPiece(Vector3 targetPos) {
        float nearLength = 10000;
        int selectNo = -1;
        for (int i = 0; i < 11; i++) {
            if (i != Global.selectMyNo) {
                if (Global.MyPiece(i) != null && nearLength > Vector3.Distance(targetPos, Global.MyPiece(i).position)) {
                    selectNo = i;
                    nearLength = Vector3.Distance(targetPos, Global.MyPiece(i).position);
                }
            }
        }
        return selectNo;
    }

    /// <summary>
    ///  ターゲット位置に最も近い相手コマのナンバーを返す(CPU用)
    /// </summary>
    /// <param name="targetPos"></param>
    /// <returns></returns>
    static public int NearVsPiece(Vector3 targetPos) {
        float nearLength = 10000;
        int selectNo = -1;
        for (int i = 0; i < 11; i++) {
            if (i != Global.selectVsNo) {
                if (Global.VsPiece(i) != null && nearLength > Vector3.Distance(targetPos, Global.VsPiece(i).position)) {
                    selectNo = i;
                    nearLength = Vector3.Distance(targetPos, Global.VsPiece(i).position);
                }
            }
        }
        return selectNo;
    }

    /// <summary>
    ///  ターゲット位置に最も近いコマの、targetNoコマに対しての距離が一定以下ならfalse
    /// </summary>
    /// <returns></returns>
    static public bool CheckNearMyPiece() {
        float nearLength = 10000;
        Vector3 targetPos = Global.MyPiece(Global.selectMyNo).position;
        for (int i = 0; i < 11; i++) {
            if (i != Global.selectMyNo) {
                if (Global.MyPiece(i) != null && nearLength > Vector3.Distance(targetPos, Global.MyPiece(i).position)) {
                    nearLength = Vector3.Distance(targetPos, Global.MyPiece(i).position);
                }
            }
        }
        if (Global.isPlayOnlineGame == false) {
            for (int i = 0; i < 11; i++) {
                if (Global.VsPiece(i) != null && nearLength > Vector3.Distance(targetPos, Global.VsPiece(i).position)) {
                    nearLength = Vector3.Distance(targetPos, Global.VsPiece(i).position);
                }
            }
        }
        if (nearLength > 0.5) {
            return true;
        } else {
            return false;
        }
    }

    /// <summary>
    /// ファール時など、ボールに近い位置のコマを移動させる
    /// </summary>
    /// <param name="maxLength"></param>
    static public void MoveNearPieceToBall(float maxLength) {
        if (Global.ball == null) {
            return;
        }
        for (int i = 0; i < 11; i++) {
            if (Global.MyPiece(i) != null && maxLength > Vector3.Distance(MyPiece(i).position, Global.ball.position)) {
                Debug.Log("(Global.ball.position - MyPiece(i).position):" + (Global.ball.position - MyPiece(i).position));
                Vector3 unitVector = (MyPiece(i).position - Global.ball.position).normalized; //単位ベクトル
                Vector3 newPosBase = Global.ball.position + (unitVector * maxLength);
                MyPiece(i).position = new Vector3(newPosBase.x, Global.GetPiecePosY(Global.IsMeOwner(), i), newPosBase.z);
            }
            if (Global.isPlayOnlineGame == false) {
                if (Global.VsPiece(i) != null && maxLength > Vector3.Distance(VsPiece(i).position, Global.ball.position)) {
                    Debug.Log("(Global.ball.position - VsPiece(i).position):" + (Global.ball.position - VsPiece(i).position));
                    Vector3 unitVector = (VsPiece(i).position - Global.ball.position).normalized; //単位ベクトル
                    Vector3 newPosBase = Global.ball.position + (unitVector * maxLength);
                    VsPiece(i).position = new Vector3(newPosBase.x, Global.GetPiecePosY(false, i), newPosBase.z);
                }
            }
        }
    }

    /// <summary>
    /// コマの動きを停止(ファール時など)
    /// </summary>
    /// <param name="myNo"></param>
    /// <param name="vsNo"></param>
    public static void PieceMoveStop(int myNo, int vsNo) {
        //Debug.Log("PieceMoveStop myNo:" + myNo + " vsNo:" + vsNo);
        if (myNo >= 0 && MyPiece(myNo) != null) {
            iTween.Stop(MyPiece(myNo).gameObject, "move");
        }
        if (vsNo >= 0 && VsPiece(vsNo) != null) {
            iTween.Stop(VsPiece(vsNo).gameObject, "move");
        }
    }

    /// <summary>
    /// ボールがフィールド外にある場合はtrue
    /// </summary>
    /// <returns></returns>
    public static bool IsBallOutOfField() {
        if (Global.ball == null) {
            return false;
        }
        Vector3 ballPos = Global.ball.position;
        if ((-Global.fieldEndX < ballPos.x && ballPos.x < Global.fieldEndX) &&
            (-Global.fieldEndZ < ballPos.z && ballPos.z < Global.fieldEndZ)) {
            return false;
        } else {
            return true;
        }
    }

    /// <summary>
    /// コマがフィールド外にある場合はtrue
    /// </summary>
    /// <param name="piecePos"></param>
    /// <returns></returns>
    public static bool IsPieceOutOfField(Vector3 piecePos) {
        if ((-Global.fieldEndX < piecePos.x && piecePos.x < Global.fieldEndX) &&
            (-Global.fieldEndZ < piecePos.z && piecePos.z < Global.fieldEndZ)) {
            return false;
        } else {
            return true;
        }
    }

    /// <summary>
    ///  時間をおいてテキストボックスを閉じる (呼び出し時にStartCoroutineを使うこと)
    /// </summary>
    /// <param name="waitSeconds"></param>
    /// <param name="boxName"></param>
    /// <returns></returns>
    public static IEnumerator CloseTextBox(float waitSeconds, string boxName) {
        yield return new WaitForSeconds(waitSeconds); //設定seconds秒数立ってから下の処理を行う
        MyCanvas myCanvas = GameObject.Find("Canvas").GetComponent<MyCanvas>();
        myCanvas.SetActive(boxName, false);
    }

    /// <summary>
    /// 現在開いているテキストボックスを閉じて少し時間を置いて開き、再度閉じる(すでにボックスが出ている際に一度閉じる演出)
    /// (呼び出し時にStartCoroutineを使うこと)
    /// </summary>
    public static IEnumerator CloseAndOpenBox(float waitSeconds, string boxName) {
        MyCanvas myCanvas = GameObject.Find("Canvas").GetComponent<MyCanvas>();
        myCanvas.SetActive(boxName, false);
        yield return new WaitForSeconds(0.1f); //設定seconds秒数立ってから下の処理を行う
        myCanvas.SetActive(boxName, true);
        yield return new WaitForSeconds(waitSeconds); //設定seconds秒数立ってから下の処理を行う
        myCanvas.SetActive(boxName, false);
    }

    /// <summary>
    /// 指定時間経過後に指定メソッド(Action)を実行。(呼び出し時にStartCoroutineを使うこと)
    /// </summary>
    public static IEnumerator WaitAndAction(float waitSeconds, Action AfterMethod) {
        Debug.Log("Global.WaitAndAction起動 waitSeconds:"+waitSeconds);
        yield return new WaitForSeconds(waitSeconds); //設定seconds秒数立ってから下の処理を行う
        AfterMethod();
    }


    /// <summary>
    /// シーンの非同期ローディング (呼び出し時にStartCoroutineを使うこと)
    /// </summary>
    /// <param name="sceneName"></param>
    /// <returns></returns>
    public static IEnumerator LoadScene(string sceneName) {
        AsyncOperation async = Application.LoadLevelAsync(sceneName);
        async.allowSceneActivation = false; // 許可されるまでシーン遷移をしない
        Debug.Log("Mainシーンの非同期ロード開始");
        while (async.progress < 0.9f) { //0.9までしか進まない(0.9でロード完了する)仕様(Unity4,5系共に同じ)
            Debug.Log("進捗度：" + async.progress);
            yield return new WaitForEndOfFrame();
        }
        Debug.Log("ロード完了");
        async.allowSceneActivation = true; // シーン遷移許可
    }

    /// <summary>
    /// URLロード時処理 (呼び出し時にStartCoroutineを使うこと)
    /// </summary>
    /// <param name="loadAction">正常読み込み時に処理する内容のメソッド</param>
    /// <param name="form">POSTフォーム</param>
    /// <param name="urlParts">URLのうち、文字列が変化する箇所</param>
    /// <param name="myCanvas">キャンバス</param>
    public static IEnumerator UrlLoad(WWWForm form, string urlParts, MyCanvas myCanvas, Action<WWW> loadAction) {
        Debug.Log("UrlLoad開始");
        WWW www = new WWW(Global.GetDomain() + Global.TopPath + urlParts, form); //POST処理
        Debug.Log("ロード開始2 www:" + www.url);
        yield return www; // wait until JSON Contents will come 
        myCanvas.SetActive("ServerErrorBox", false);
        Debug.Log("ロード開始3");
        if (Global.isServerSuccess(www)) {
            Debug.Log("サーバ読み込み成功");
            loadAction(www);
            yield break; //コルーチンの終了
        } else {
            Debug.Log("サーバ読み込み失敗");
            myCanvas.SetActive("ServerErrorBox", true);
            myCanvas.SetActive("LoadImage", false);
            if (urlParts == "android/receipt" || urlParts == "ios/receipt") {
                yield return new WaitForSeconds(3f); //設定seconds秒数立ってから下の処理を行う
                myCanvas.SetActive("ServerErrorBox", false);
            }
        }
    }

    /// <summary>
    /// サーバアクセス結果をチェックし、アクセスに成功した際はtrueを返す
    /// </summary>
    public static bool isServerSuccess(WWW www) {
        if (www.error != null || string.IsNullOrEmpty(www.text.Trim()) || www.text.Contains("DOCTYPE")) {
            return false; //サーバエラー
        } else {
            return true;
        }
    }

    /// <summary>
    /// ボール移動に伴うカメラ移動処理
    /// </summary>
    /// <param name="ballPosDiffX"></param>
    /// <param name="ballPosDiffZ"></param>
    public static void MoveCamera(float ballPosDiffX, float ballPosDiffZ) {
        //Debug.Log("MoveCamera ballPosDiff X:" + ballPosDiffX + " Z:" + ballPosDiffZ +
        //    " camera.position x:" + camera.position.x + " z:" + camera.position.z);
        if ((0 <= ballPosDiffX && mainCamera.position.z < 3) || (ballPosDiffX < 0 && -3 < mainCamera.position.x)) {
            mainCamera.position += new Vector3(ballPosDiffX, 0, 0); //x=-3~3の範囲でカメラ位置調整
        }
        if ((0 <= ballPosDiffZ && mainCamera.position.z < 5) || (ballPosDiffZ < 0 && -7 < mainCamera.position.z)) {
            mainCamera.position += new Vector3(0, 0, ballPosDiffZ); //z=-7~5の範囲でカメラ位置調整
        }
    }

    /// <summary>
    ///  ボール移動量より、ボールの回転量を返す。
    /// </summary>
    /// <param name="pieceEndToBallDistance">コマの移動地点とボール位置との差(ボール移動量)</param>
    public static float BallRoll(float pieceEndToBallDistance) {
        if (pieceEndToBallDistance > 5) {
            pieceEndToBallDistance = 5;
        }
        return pieceEndToBallDistance * 180f; //ボールの回転量(角度℃)
    }

    /// <summary>
    /// ボールのiTween以外の動作を停止させる
    /// </summary>
    public static void BallSpeedZero() {
        if (Global.ball != null) {
            Global.ball.rigidbody.isKinematic = false;
            Global.ball.rigidbody.velocity = Vector3.zero;
            Global.ball.rigidbody.angularVelocity = Vector3.zero;

            //Global.nowMoveBall = false;
            //Global.nowBallTweenActive = false;
        }
    }

    /// <summary>
    /// ゲームサイドを反対側に移す。どちらのサイドでもないときはエラー
    /// </summary>
    public static void ChangeSide() {
        Global.noTouchTime = 0;
        Global.isMyTurnMoveOneOver = false;

        if (Global.selectMyNo >= 0 && Global.MyPiece(Global.selectMyNo) != null) {
            iTween.Stop(Global.MyPiece(Global.selectMyNo).gameObject, "move");
        }
        if (Global.selectVsNo >= 0 && Global.VsPiece(Global.selectVsNo) != null) {
            iTween.Stop(Global.VsPiece(Global.selectVsNo).gameObject, "move");
        }
        Global.selectMyNo = -1;
        Global.selectVsNo = -1;
        Global.nowMoveMe = false;
        Global.nowMoveOpponent = false;
        Global.nowBallTweenActive = false;

        if (Global.NowMySide()) {
            Debug.Log("ChangeSide ゲームサイド 守備側に変更");
            Global.gameSide = Global.Side.Opponent;
            Global.nowPlayerTweenActive = false;
            Global.nowCpuTweenActive = false;
        } else if (Global.NowOpponentSide()) {
            Debug.Log("ChangeSide ゲームサイド 攻撃側に変更");
            Global.gameSide = Global.Side.My;
            Global.nowPlayerTweenActive = false;
            Global.nowCpuTweenActive = false;
        } else {
            Debug.Log("changeSide エラー発生");
        }
    }

    /// <summary>
    /// クライアント用のゲームサイドチェンジ(online)
    /// </summary>
    /// <param name="isOffenseOwnerSide"></param>
    public static void ChangeSide2TestTest(bool isOffenseOwnerSide) {
        Global.isMyTurnMoveOneOver = false;
        if (isOffenseOwnerSide == false) {
            Debug.Log("ChangeSide ゲームサイド 攻撃側に変更");
            Global.gameSide = Global.Side.My;
            Global.nowPlayerTweenActive = false;
            Global.nowCpuTweenActive = false;
        } else {
            Debug.Log("ChangeSide ゲームサイド 守備側に変更");
            Global.gameSide = Global.Side.Opponent;
            Global.nowPlayerTweenActive = false;
            Global.nowCpuTweenActive = false;
        }
    }

    /// <summary>
    /// 選択されていないコマのレイヤ判定を変更する。
    /// maskStr = "Default" ならタッチ判定あり、"Ignore Raycast"ならタッチ判定なしとなる
    /// </summary>
    /// <param name="maskStr"></param>
    public static void OtherPieceLayerChange(string maskStr) {
        //Debug.Log("レイヤ判定変更：" + maskStr);
        for (int i = 0; i < 11; i++) {
            if (i != Global.selectMyNo) {
                if (Global.MyPiece(i).gameObject.layer != LayerMask.NameToLayer(maskStr)) {
                    Global.MyPiece(i).gameObject.layer = LayerMask.NameToLayer(maskStr);
                }
            }
        }
    }

    /// <summary>
    /// ゲーム終了(通信切断)時の処理。処理抜けが無いか注意
    /// </summary>
    public static void GameReset() {
        Global.isPlayOnlineGame = false;
        Global.isSetupOnlineGame = false;
        Global.isRoomOwner = false;
        Global.isFriendBattle = false;
        Global.roomNum = -1;
        Global.nowOpponentDisconnect = false;

        if (Global.selectMyNo >= 0 && Global.MyPiece(Global.selectMyNo) != null) {
            iTween.Stop(Global.MyPiece(Global.selectMyNo).gameObject, "move");
        }
        if (Global.selectVsNo >= 0 && Global.VsPiece(Global.selectVsNo) != null) {
            iTween.Stop(Global.VsPiece(Global.selectVsNo).gameObject, "move");
        }

        /* ゲーム進行関連処理 */
        Global.runTime = 0;
        Global.nowGameSetup = false; //試合開始前、選手コマのセットアップ中はtrue
        Global.firstGameSide = Side.No; //試合開始時の攻撃側を格納 
        Global.nextGameSide = Side.No; //どちらかの得点時に、次のキックオフサイドを記録。それ以外のときはNO
        Global.isPrepareKickoff = false; //キックオフの準備中(選手配置時)はtrue
        Global.playerScore = 0; //プレイヤーの試合得点
        Global.opponentScore = 0; //CPUの試合得点
        Global.isGameActive = false; //ゲーム停止中以外はtrue
        Global.ballLastHitSide = Side.No; //Myなら味方側が最後にボールに接触、OPPONENTなら敵側が最後にボールに接触
        Global.cpuLevel = 3; //CPUの強さレベル 数値が大きい程強い。デフォルト値を3にしている
        Global.isMyTurnMoveOneOver = false; //新しく攻撃ターンになった後に1度以上移動したらtrue(local適用。ターンが交代する度falseにする)
        Global.noTouchTime = 0; //攻撃側が移動しないまま経過している時間
        Global.nowGoal = false; //得点発生直後はtrue
        Global.nowGameEnd = false; //試合終了時にはtrue
        Global.gameSide = Side.No;

        /* ボールのフィールド外処理 */
        Global.throwinSide = Side.No; //スローイン処理時以外はNO
        Global.throwinLineZ = -100; //スローイン発生時にボールが通過したラインZ位置(発生時以外は-100とする)
        Global.goalkickSide = Side.No;//ゴールキック処理の状況 
        Global.cornerkickSide = Side.No;//コーナーキック処理の状況
        Global.cornerkickPoint = 0; //平常時は0,コーナーキックの際はそれぞれの角で1-4(1は敵側左、2は味方側左、3は敵側右、4は味方側右)
        Global.goalLineOutPoint = 0; //平常時は0、ボールがゴールラインを割った際は1-4(オーナー視点で、1は敵側左、2は敵側右、3は味方側左、4は味方側右)
        Global.isFirstReturnKick = false; //フィールド外から復帰した最初のショット(スローイン、コーナーキック時に使用)

        /* ボール・コマ移動関連 */
        Global.isFoulStartNow = false;
        Global.nowMoveBall = false; //ボール移動中はtrue
        Global.nowMoveMe = false; //プレイヤーコマ移動中はtrue
        Global.nowMoveOpponent = false; //相手側が移動中はtrue
        Global.selectMyNo = -1; //タッチしたプレーヤーコマの番号(0-10)を保存 -1の時は該当なし 
        Global.selectVsNo = -1; //選択中の対戦相手コマ
        Global.canDefenseMove = false; //守備側として動作可能なタイミングではtrue(攻撃側移動開始時より、1度移動可能)
        Global.nowGoalKick = false; //プレーヤーのゴールキックになったときにtrue
        Global.nowPlayerHitWall = false; //プレイヤーコマが壁やゴールポストに衝突したらtrue
        Global.touchLinePos = new Vector3(0, 0, 0); //コマがライン外に出た際の座標
        Global.nowLineOut = false; //コマがライン外に出た際にtrue

        /* iTween動作 */
        Global.nowPlayerTweenActive = false; //プレイヤーコマ動作のiTweenがアクティブならtrue
        Global.nowCpuTweenActive = false; //CPUコマ動作のiTweenがアクティブならtrue
        Global.nowBallTweenActive = false; //ボール動作のiTweenがアクティブならtrue

        Global.step = Global.GameStep.Normal; //追加
    }

    /// <summary>
    /// 文字列をBase64エンコード(PlayerPrefsにおける日本語文字列の保存対策)
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public static string B64Encode(string s) {
        return System.Convert.ToBase64String(UTF8Encoding.UTF8.GetBytes(s));
    }

    /// <summary>
    /// Base64文字列をデコード(PlayerPrefsにおける日本語文字列の保存対策)
    /// </summary>
    /// <param name="s"></param>
    /// <returns></returns>
    public static string B64Decode(string s) {
        return UTF8Encoding.UTF8.GetString(System.Convert.FromBase64String(s));
    }

    public const int CountryNum = 12; //選択できる国の数
    /// <summary>
    /// チームNoより国名を取得(チームのランダム選択時の処理に数値が必要なため、数値で管理している。)
    /// </summary>
    /// <param name="teamNo">0~10までそれぞれ国名と対応。-1なら"Random"と返す</param>
    public static string CountryName(int teamNo) {
        switch (teamNo) {
            case -1: return "Random";
            case 0: return "Argentina";
            case 1: return "Brazil";
            case 2: return "England";
            case 3: return "France";
            case 4: return "Germany";
            case 5: return "Italy";
            case 6: return "Japan";
            case 7: return "Mexico";
            case 8: return "Netherlands";
            case 9: return "Russia";
            case 10: return "Singapore";
            case 11: return "Spain";
            default: return "Argentina"; //エラー時処理
        }
    }

    /// <summary>
    /// 2P(Client)の場合、ユニフォーム設定時に2Pを付与
    /// </summary>
    public static string ClientTail() {
        return "2P";
    }

    /// <summary>
    /// スプライト画像のセット
    /// </summary>
    /// <param name="texturePath">使用画像のパス</param>
    /// <param name="objectPath">画像貼り付け対象オブジェクトのパス</param>
    public static void SpriteImageSet(string texturePath, string objectPath) {
        //Debug.Log("SpriteImageSet texturePath:" + texturePath + " objectPath:" + objectPath);
        Texture2D texture = Resources.Load(texturePath) as Texture2D; //使用するテクスチャ画像
        Image img = GameObject.Find(objectPath).GetComponent<Image>(); //画像を貼り付ける対象オブジェクト
        img.sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
    }

    //ログヘルパー。参照元が分からなくなるので極力使わず、デバッグ上処理が重いLogに使う
    public static void LogHelp(string text) {
        if (Debug.isDebugBuild) {
            Debug.Log(text);
        }
    }

    /// <summary>
    /// viewIDより自コマを検索(オンラインのみ)
    /// </summary>
    /// <param name="viewId"></param>
    /// <returns></returns>
    public static string searchMyPieceByViewId(int viewId) {
        int[] vsViewIds = Global.myViewIds;
        for (int i = 0; i < 11; i++) {
            if (viewId == vsViewIds[i]) {
                Debug.Log("ヒット！ viewId:" + viewId + " i:" + i);
                return Global.MyPieceNameBase + i;
            }
        }
        return "";
    }

    public static float PieceMoveTime(float moveLength) {
        float time = 0.5f + moveLength * 0.45f;
        return time;
    }

    public static void PieceMoveTween(GameObject piece, Vector3 endPos, float moveLength, GameObject target) {
        iTween.MoveTo(piece, iTween.Hash(
            "position", endPos,
            "easeType", Global.EaseType,
            "time", PieceMoveTime(moveLength),
            "oncomplete", "StopPiece",
            "oncompletetarget", target)); //コマ移動
    }

    //oncomplete以外はPieceMoveTweenと同じ
    public static void CpuMoveTween(GameObject piece, Vector3 endPos, float moveLength, GameObject target) {
        iTween.MoveTo(piece, iTween.Hash(
            "position", endPos,
            "easeType", Global.EaseType,
            "time", PieceMoveTime(moveLength),
            "oncomplete", "StopCPU",
            "oncompletetarget", target)); //コマ移動
    }

#if UNITY_ANDROID
    /// <summary>
    /// バージョンネームを取得する
    /// PlayerSettings上では[ Bundle Version ]の値
    /// </summary>
    public static string GetAndroidAppVersionName() {
        AndroidJavaObject pInfo = GetPackageInfo();
        string versionName = pInfo.Get<string>("versionName");
        return versionName;
    }

    /// <summary>
    /// Version情報を保持しているPackageInfoクラスを取得する
    /// </summary>
    public static AndroidJavaObject GetPackageInfo() {
        AndroidJavaClass unityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
        AndroidJavaObject context = unityPlayer.GetStatic<AndroidJavaObject>("currentActivity").Call<AndroidJavaObject>("getApplicationContext");
        AndroidJavaObject pManager = context.Call<AndroidJavaObject>("getPackageManager");
        AndroidJavaObject pInfo = pManager.Call<AndroidJavaObject>("getPackageInfo", context.Call<string>("getPackageName"), pManager.GetStatic<int>("GET_ACTIVITIES"));
        return pInfo;
    }
#endif //UNITY_ANDROID
    
    /// <summary>
    /// iOSならAppStoreのアプリページ、AndroidならGoogle Playのアプリページへ飛ばす
    /// </summary>
    public static void OpenStorePage() {
#if UNITY_ANDROID
        Application.OpenURL("https://play.google.com/store/apps/details?id=jp.co.musicalab.flick.soccer");
#elif UNITY_IPHONE
        //動作チェック必要
        Application.OpenURL("https://itunes.apple.com/jp/app/flick-table-soccer-subbuteo/id1086016701?l=ja&ls=1&mt=8");
#endif
    }

    //タッチ終了時にtrue
    static public bool IsTouchNow() {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Ended) { //タッチ終了判定
            return true;
        } else if (Input.GetMouseButtonUp(0)) { //マウス判定(Editor用)
            return true;
        }
        return false;
    }


}

